_id: 5tsZI6zk29yX1ckA
_key: '!items!5tsZI6zk29yX1ckA'
img: systems/pf1/icons/items/inventory/lockpick.jpg
name: Rogue (Unchained)
system:
  armorProf:
    value:
      - lgt
  bab: med
  classSkills:
    acr: true
    apr: true
    art: true
    blf: true
    clm: true
    crf: true
    dev: true
    dip: true
    dis: true
    esc: true
    int: true
    kdu: true
    klo: true
    lin: true
    lor: true
    per: true
    prf: true
    pro: true
    sen: true
    slt: true
    ste: true
    swm: true
    umd: true
  description:
    value: >-
      <p>Thief, sneak, charmer, diplomat—all of these and more describe the
      rogue. When not skulking in the shadows, disarming traps, and stealing
      from the unaware, rogues may rub shoulders with powerful nobles or plot
      capers with fellow crooks. The rogue is the master of many faces, using
      her skills and talents to get herself both into and out of trouble with
      rakish aplomb. While others may call them charlatans and burglars, even
      the most larcenous rogues tend to consider themselves consummate
      professionals, willing to take on any job if the price is
      right.</p><p><strong>Unchained:</strong> While much of the unchained rogue
      will be familiar to those who have played the class from the Core
      Rulebook, there are a number of new class features that greatly enhance
      the power and flexibility of the rogue. Chief among these is the
      debilitating injury class feature. A rogue with this ability can severely
      hamper her foes, giving her a much-needed boost to her offense or defense,
      depending on the situation. In addition, with finesse training, the rogue
      now gains Weapon Finesse for free at 1st level. This ability also lets her
      add her Dexterity to damage rolls with one weapon starting at 3rd level.
      Finally, the rogue’s edge ability ties into a new system presented in
      Chapter 2 of this book called skill unlocks. With this feature, the rogue
      can master a small set of chosen skills, outperforming all those
      characters without access to such talents.</p><h3><strong>Class
      Skills</strong></h3><p>The rogue (unchained)’s class skills are
      <em>Acrobatics</em> (<em>Dex</em>), <em>Appraise</em> (<em>Int</em>),
      <em>Bluff</em> (<em>Cha</em>), <em>Climb</em> (<em>Str</em>),
      <em>Craft</em> (<em>Int</em>), <em>Diplomacy</em> (<em>Cha</em>),
      <em>Disable Device</em> (<em>Dex</em>), <em>Disguise</em> (<em>Cha</em>),
      <em>Escape Artist</em> (<em>Dex</em>), <em>Intimidate</em> (<em>Cha</em>),
      <em>Knowledge</em> (dungeoneering) (<em>Int</em>), <em>Knowledge</em>
      (local) (<em>Int</em>), <em>Linguistics</em> (<em>Int</em>),
      <em>Perception</em> (<em>Wis</em>), <em>Perform</em> (<em>Cha</em>),
      <em>Profession</em> (<em>Wis</em>), <em>Sense Motive</em> (<em>Wis</em>),
      <em>Sleight of Hand</em> (<em>Dex</em>), <em>Stealth</em> (<em>Dex</em>),
      <em>Swim</em> (<em>Str</em>), and <em>Use Magic Device</em>
      (<em>Cha</em>).</p>
  links:
    classAssociations:
      - level: 1
        uuid: Compendium.pf1.class-abilities.HmNDcFTlaEqr60Vw
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.rg0FL5INDBUt2oSK
      - level: 1
        uuid: Compendium.pf1.class-abilities.Item.pEODJDoTk7uhCZY7
      - level: 2
        uuid: Compendium.pf1.class-abilities.KQYCRLEdD4bGA5ak
      - level: 2
        uuid: Compendium.pf1.class-abilities.iBTrvPtn3jczInbn
      - level: 3
        uuid: Compendium.pf1.class-abilities.sTlu3zgAEDdJnER5
      - level: 4
        uuid: Compendium.pf1.class-abilities.nL9Ds9nflmID84vo
      - level: 4
        uuid: Compendium.pf1.class-abilities.7WaQxnVaaoL4AGr8
      - level: 5
        uuid: Compendium.pf1.class-abilities.iyYyc3rKW0cBZ1cs
      - level: 8
        uuid: Compendium.pf1.class-abilities.ZfnHhhTFQVo0Lj4P
      - level: 10
        uuid: Compendium.pf1.class-abilities.EQ7JGo4P1XirLO46
      - level: 20
        uuid: Compendium.pf1.class-abilities.9l1EOZ2l3wLudvJN
  savingThrows:
    ref:
      value: high
  skillsPerLevel: 8
  sources:
    - id: PZO1131
      pages: 20-24
  tag: rogueUnchained
  wealth: 4d6 * 10
  weaponProf:
    custom:
      - Hand Crossbow
      - Rapier
      - Sap
      - Shortsword
      - Shortbow
    value:
      - simple
type: class

