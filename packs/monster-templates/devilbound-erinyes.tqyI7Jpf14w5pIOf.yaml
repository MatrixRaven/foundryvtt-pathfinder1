_id: tqyI7Jpf14w5pIOf
_key: '!items!tqyI7Jpf14w5pIOf'
img: systems/pf1/icons/skills/violet_07.jpg
name: Devilbound (Erinyes)
system:
  changes:
    - _id: vcdypy0m
      formula: '4'
      target: nac
      type: untyped
    - _id: losczp5p
      formula: '2'
      target: con
      type: untyped
    - _id: 6t9t7o16
      formula: '2'
      target: dex
      type: untyped
    - _id: 4l8r8z2h
      formula: '2'
      target: cha
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>A devilbound creature has made a
      bargain with a devil, promising a service and its soul in exchange for
      infernal power. The specific service depends on the devil’s type and
      motivations, but always furthers the interests of Hell.<p>“Devilbound
      creature” is an acquired template that can be added to any creature with 5
      or more Hit Dice and Intelligence, Wisdom, and Charisma scores of 3 or
      higher (referred to hereafter as the base creature). The creature retains
      all the base creature’s statistics and special abilities except as noted
      here.

      <p><b>CR:</b> Same as the base creature +1.</p>

      <p><b>Alignment:</b> Any evil. A devilbound creature radiates an evil aura
      as if it were an evil outsider.</p>

      <p><b>Senses:</b> A devilbound creature gains darkvision 60 ft. and the
      see in darkness ability.</p>

      <p><b>Armor Class:</b> Natural armor improves by +4.</p>

      <p><b>Defensive Abilities:</b> A devilbound creature gains a +4 bonus on
      saving throws against poison, resist fire 30, and regeneration 5 (good
      spells, good weapons).</p>

      <p><b>Weaknesses:</b> The devil-bound creature gains the following
      weakness.</p>

      <p><em>Contract Bound (Ex): </em>The creature has signed a contract of
      service in return for this template. The devil must reveal its nature as a
      creature of Hell when it offers a contract, and it can’t hide the details
      of the contract in any way. The creature must enter the agreement
      willingly (without magical compulsion). Usually the creature must perform
      one or more tasks for the devil, and in exchange the creature gains the
      template’s abilities, whether immediately, after a specific amount of
      time, or once the tasks are completed.<p>The contract always includes a
      clause that damns the creature’s soul to Hell when the creature dies, with
      credit for the act and possession of the soul going to the devil signing
      the contract. When the creature dies, its soul is automatically imprisoned
      in a gem, which immediately appears in Hell as one of the devil’s
      belongings. If the devil is dead when the creature dies, the creature’s
      soul is destroyed, and can’t be restored to life except by miracle or
      wish. If the creature fails to perform the tasks in the allotted time, its
      soul is still damned and the devil is not obligated to provide the
      promised abilities.<p>Many contracts state that the devil, its agents, and
      its allies will not attempt to kill the creature. This doesn’t protect
      against all devils, but does offer the creature a measure of protection
      against treachery from the signatory devil.<p>Breaking a contract with a
      devil is difficult and dangerous. Furthermore, as long as the contract
      remains in effect, a slain victim can’t be restored to life after death
      except by a miracle or wish. If the devilbound creature is restored to
      life, the devil immediately senses the name and location (as discern
      location) of the creature responsible.<p><b>Special Attacks:</b> The
      creature gains the summon universal monster ability and can summon a devil
      once per day with a 100% chance of success. The devil remains for 1 hour.
      The creature’s caster level or Hit Dice, whichever is higher, determines
      the most powerful kind of devil it can summon and the effective spell
      level of this ability, according to the following table.<br><br></p>

      <table>

      <tbody>

      <tr>

      <td><b>Caster Level</b></td>

      <td><b>Devil</b></td>

      <td><b>Spell Level</b></td>

      </tr>

      <tr>

      <td>3rd</td>

      <td>Lemure</td>

      <td>2nd</td>

      </tr>

      <tr>

      <td>9th</td>

      <td>Bearded devil</td>

      <td>5th</td>

      </tr>

      <tr>

      <td>11th</td>

      <td>Erinyes</td>

      <td>6th</td>

      </tr>

      <tr>

      <td>13th</td>

      <td>Bone devil</td>

      <td>7th</td>

      </tr>

      <tr>

      <td>15th</td>

      <td>Barbed devil</td>

      <td>8th</td>

      </tr>

      <tr>

      <td>17th</td>

      <td>Ice devil</td>

      <td>9th</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Spell-Like Abilities:</b> The creature gains the following
      spell-like abilities, depending on the kind of devil it is bound to. The
      creature uses its Hit Dice or caster level, whichever is higher, as the
      caster level for its spell-like abilities. Save DCs are based on the
      creature’s Intelligence, Wisdom, or Charisma, whichever is
      highest.<p>Accuser: 3/day—clairaudience/clairvoyance, invisibility (self
      only), summon swarm<br>Barbed: 3/day—hold monster<br>Bearded:
      3/day—dimension door, rage<br>Belier: 3/day—charm monster<br>Bone:
      3/day—fly, invisibility (self only)<br>Contract: 3/day—bestow curse,
      detect thoughts, locate creature<br>Drowning: 3/day—hydraulic pushAPG,
      water breathing<br>Erinyes: 3/day—fear (single target), unholy
      blight<br>Handmaiden: 3/day—black tentacles; 1/day—true seeing<br>Horned:
      3/day—dispel good, fireball<br>Host: 3/day—dimension door, fly<br>Ice:
      3/day—cone of cold, ice storm<br>Immolation: 3/day—fire shield,
      fireball<br>Imp: 3/day—invisibility (self only), polymorph (self only,
      same size as base creature)<br>Nemesis: 3/day—invisibility, scorching ray;
      1/day—blasphemy<br>Pit Fiend: 3/day—quickened fireball, invisibility;
      1/day—blasphemy<p><b>Abilities:</b> Adjust the base creature’s ability
      scores according to the kind of devil it is bound to.<br><br></p>

      <table>

      <tbody>

      <tr>

      <td><b>Devil</b></td>

      <td><b>Str</b></td>

      <td><b>Dex</b></td>

      <td><b>Con</b></td>

      <td><b>Int</b></td>

      <td><b>Wis</b></td>

      <td><b>Cha</b></td>

      </tr>

      <tr>

      <td>Accuser</td>

      <td>—</td>

      <td>2</td>

      <td>2</td>

      <td>—</td>

      <td>2</td>

      <td>—</td>

      </tr>

      <tr>

      <td>Barbed, bearded, host</td>

      <td>2</td>

      <td>2</td>

      <td>2</td>

      <td>—</td>

      <td>—</td>

      <td>—</td>

      </tr>

      <tr>

      <td>Belier</td>

      <td>—</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      <td>2</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Bone, ice</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      <td>2</td>

      <td>2</td>

      <td>—</td>

      </tr>

      <tr>

      <td>Contract, handmaiden</td>

      <td>—</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      <td>2</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Drowning, horned</td>

      <td>2</td>

      <td>2</td>

      <td>—</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Erinyes</td>

      <td>—</td>

      <td>2</td>

      <td>2</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Immolation</td>

      <td>2</td>

      <td>—</td>

      <td>2</td>

      <td>—</td>

      <td>—</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Imp</td>

      <td>—</td>

      <td>2</td>

      <td>—</td>

      <td>2</td>

      <td>—</td>

      <td>2</td>

      </tr>

      <tr>

      <td>Nemesis, pit fiend</td>

      <td colspan="6">+2 to any three different ability scores</td>

      </tr>

      </tbody>

      </table>
  subType: template
type: feat

