_id: APWjD9OEpFnukZm1
_key: '!items!APWjD9OEpFnukZm1'
img: systems/pf1/icons/skills/violet_07.jpg
name: Nosferatu
system:
  changes:
    - _id: isls6iyj
      formula: '8'
      target: nac
      type: untyped
    - _id: shji4pqx
      formula: '2'
      target: str
      type: untyped
    - _id: hsaye0th
      formula: '4'
      target: dex
      type: untyped
    - _id: x9jrk6el
      formula: '2'
      target: int
      type: untyped
    - _id: r9laonzt
      formula: '6'
      target: wis
      type: untyped
    - _id: rtqig562
      formula: '4'
      target: cha
      type: untyped
    - _id: n016oult
      formula: '8'
      target: skill.per
      type: racial
    - _id: xa3bldxb
      formula: '8'
      target: skill.sen
      type: racial
    - _id: kmwsb1np
      formula: '8'
      target: skill.ste
      type: racial
  crOffset: '2'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Nosferatu are savage undead who may
      be the progenitors of the common, more refined vampires. The curse of the
      nosferatu lacks the elegance and romance of its modern form, harkening to
      a forgotten age of verminous hunger and eerie powers. Granted immortal
      life but not immortal youth, nosferatu are withered, embittered creatures
      unable to create others of their kind, as they somehow lost that ability
      long ago.<p>Their ancient sensibilities still reflect the cruelty of
      epochs past, and their age-spanning plots are untethered by the modern
      affliction of morality. Nosferatu resent common vampires (which they call
      “moroi,” an ancient term from a lost language) for their beauty, whereas
      those vampires scorn the nosferatu as bestial relics of an earlier age,
      best hidden away in remote ruins so as not to sully the charismatic
      reputation of “true” vampires.<p>Because nosferatu can’t create spawn, any
      nosferatu in existence are very old—created long ago in a time before they
      lost the ability to infect others with their undead curse. Most nosferatu
      live in isolated places with few visitors, and a nosferatu could be a
      thousand years old and yet have fewer than a dozen character levels
      because it lacks sufficient foes to challenge it or the initiative to
      train itself.<p>“Nosferatu” is an acquired template that can be added to
      any living creature with 5 or more Hit Dice (referred to hereafter as the
      base creature). Most nosferatu were once humanoids, fey, or monstrous
      humanoids. A nosferatu uses the base creature’s stats and abilities except
      as noted here.<p><b>CR:</b> Same as the base creature +2.</p>

      <p><b>Alignment:</b> Any evil.</p>

      <p><b>Type:</b> The creature’s type changes to undead (augmented). Do not
      recalculate class Hit Dice, BAB, or saves.</p>

      <p><b>Senses:</b> A nosferatu gains darkvision 60 ft., low-light vision,
      and scent.</p>

      <p><b>Armor Class:</b> Natural armor improves by 8.</p>

      <p><b>Hit Dice:</b> Change all racial Hit Dice to d8s. Class Hit Dice are
      unaffected. As an undead, a nosferatu uses its Charisma modifier to
      determine its bonus hit points (instead of Constitution).</p>

      <p><b>Defensive Abilities:</b> A nosferatu gains channel resistance +4 and
      DR 5/wood and piercing (this includes all wood-shafted weapons such as
      arrows, crossbow bolts, spears, and javelins, even if the weapon’s actual
      head is made of another material). It also gains resistance 10 to cold,
      electricity, and sonic.<p>A nosferatu gains fast healing 5. If reduced to
      0 hit points in combat, a nosferatu assumes its swarm form (see below) and
      attempts to escape. It must reach its coffin within 1 hour or be utterly
      destroyed. (In swarm form, it can normally travel up to 5 miles in 1
      hour.) Additional damage dealt to a nosferatu forced into swarm form has
      no effect. Once at rest, the nosferatu is helpless. It regains 1 hit point
      after 1 hour, then is no longer helpless and resumes healing at the rate
      of 5 hit points per round.<p><b>Weaknesses:</b> A nosferatu can’t tolerate
      the strong odor of garlic, and won’t enter an area laced with it.
      Similarly, it recoils from mirrors or strongly presented holy symbols.
      These things don’t harm the nosferatu—they merely keep it at bay. A
      recoiling nosferatu must stay at least 5 feet away from the mirror or holy
      symbol and can’t touch or make melee attacks against that creature.
      Holding a nosferatu at bay takes a standard action. After 1 round, a
      nosferatu can overcome its revulsion of the object and function normally
      each round it succeeds at a DC 25 Will save.<p>A nosferatu cannot enter a
      private home or dwelling unless invited in by someone with the authority
      to do so.<p>Reducing a nosferatu’s hit points to 0 incapacitates it but
      doesn’t always destroy it (see fast healing). However, certain attacks can
      slay nosferatu. Exposing any nosferatu to direct sunlight staggers it on
      the first round of exposure and destroys it utterly on the second
      consecutive round of exposure if it does not escape. Each round of
      immersion in running water deals an amount of damage to a nosferatu equal
      to one-third of its full normal hit points—a nosferatu reduced to 0 hit
      points in this manner is destroyed. Driving a wooden stake through a
      helpless nosferatu’s heart instantly slays it (this is a full-round
      action). However, it returns to life if the stake is removed, unless its
      head is also severed and anointed with holy water.<p><b>Speed:</b> Same as
      the base creature. If the base creature has a swim speed, the nosferatu is
      not harmed by running water.</p>

      <p><b>Melee:</b> A nosferatu gains two claw attacks if the base creature
      didn’t have any (1d4 points of damage for a Small nosferatu, 1d6 points of
      damage for a Medium one).</p>

      <p><b>Special Attacks:</b> A nosferatu gains several special attacks. Its
      save DCs are equal to 10 + 1/2 the nosferatu’s Hit Dice + the nosferatu’s
      Cha modifier unless otherwise noted.<p><em>Blood Drain (Ex): </em>A
      nosferatu can suck blood from a helpless, willing, or grappled living
      victim with its fangs by making a successful grapple check. If it pins the
      foe, it drains blood, draining 1d4 points of Constitution and Wisdom each
      round the pin is maintained. On each round it drains blood, the nosferatu
      gains 5 temporary hit points that last for 1 hour (up to a maximum number
      of temporary hit points equal to its full normal hit
      points).<p><em>Dominate (Su): </em>A nosferatu can crush a humanoid
      opponent’s will as a standard action. Anyone the nosferatu targets must
      succeed at a Will save or fall instantly under the nosferatu’s influence,
      as though by a dominate person spell (caster level 12th). This ability has
      a range of 30 feet. At the GM’s discretion, some nosferatu (such as a very
      old one or with an unusually strong bloodline) might be able to affect
      different creature types with this power.<p><em>Telekinesis (Su): </em>As
      a standard action, a nosferatu can use telekinesis (caster level
      12th).<p><b>Special Qualities:</b> A nosferatu gains the
      following.<p><em>Spider Climb (Ex): </em>A nosferatu can climb sheer
      surfaces as though under the effects of a spider climb spell.<p><em>Swarm
      Form (Su): </em>As a standard action, a nosferatu can change into a bat
      swarm, centipede swarm, rat swarm, or spider swarm. The nosferatu gains
      the natural weapons and extraordinary special attacks of the swarm it has
      transformed into. The swarm has the same number of hit points as the
      nosferatu. While in swarm form, a nosferatu can’t use its claw attacks or
      any of its special attacks. It retains the defensive abilities,
      weaknesses, and special qualities it gains from being a nosferatu, counts
      as an undead creature, and can use any of the swarm’s abilities and
      defenses. It can remain in swarm form until it assumes another form or
      until the next sunrise.<p><em>Telepathy (Su): </em>A nosferatu can
      communicate telepathically with any creature within 60 feet that speaks
      the same language it does. In addition, a nosferatu can use this ability
      to communicate with any animal, magical beast, or vermin.<p><b>Ability
      Scores:</b> Str +2, Dex +4, Int +2, Wis +6, Cha +4. As an undead creature,
      a nosferatu has no Constitution score.</p>

      <p><b>Skills:</b> A nosferatu gains a +8 racial bonus on Perception, Sense
      Motive, and Stealth checks.</p>

      <p><b>Feats:</b> A nosferatu gains Alertness, Improved Initiative,
      Lightning Reflexes, and Skill Focus (in two different skills) as bonus
      feats.</p>
  subType: template
type: feat

