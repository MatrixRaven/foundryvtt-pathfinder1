_id: xVZlhotkGUaKriMi
_key: '!items!xVZlhotkGUaKriMi'
img: systems/pf1/icons/skills/violet_07.jpg
name: Guardian Spirit
system:
  crOffset: '0'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>A guardian spirit is bound to the
      fate of a mortal being (called its “ward”). This bond may be formed by any
      number of beings or events carrying the weight of destiny, such as
      deities, the Eldest, norns, and mythic creatures and magic. A spirit can
      bind itself willingly if it believes that doing so is likely to further
      its agenda, give it more power, or allow it access to the world of
      mortals. Mortals can generally invoke a guardian spirit only with
      summoning and calling spells.<p>“Guardian spirit” is an acquired template
      that can be added to any fey or outsider that qualifies to become a
      familiar through the Improved Familiar feat (this template does not make
      it a familiar, however). A guardian spirit uses all the base creature’s
      statistics and special abilities except as noted here. A guardian spirit
      has a rune on its forehead similar to that on an eidolon (though its ward
      does not gain a matching rune).</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Conjuration Spell Level</b></td>

      <td><b>CR</b></td>

      <td><b>Armor Class</b></td>

      <td><b>Hit Dice</b></td>

      <td><b>Ability Scores</b></td>

      <td><b>Special</b></td>

      </tr>

      <tr>

      <td>3</td>

      <td>+0</td>

      <td>+0</td>

      <td>+0</td>

      <td>+2</td>

      <td>Smite threat 1/day, spell-like abilities</td>

      </tr>

      <tr>

      <td>4</td>

      <td>+2</td>

      <td>+2</td>

      <td>+2</td>

      <td>+2</td>

      <td>Fated guardian, spell-like ability</td>

      </tr>

      <tr>

      <td>5</td>

      <td>+4</td>

      <td>+4</td>

      <td>+4</td>

      <td>+4</td>

      <td>Spell-like ability</td>

      </tr>

      <tr>

      <td>6</td>

      <td>+6</td>

      <td>+6</td>

      <td>+6</td>

      <td>+4</td>

      <td>Smite threat 2/day, spell-like ability</td>

      </tr>

      <tr>

      <td>7</td>

      <td>+8</td>

      <td>+8</td>

      <td>+8</td>

      <td>+6</td>

      <td>Spell-like ability</td>

      </tr>

      <tr>

      <td>8</td>

      <td>+10</td>

      <td>+10</td>

      <td>+10</td>

      <td>+6</td>

      <td>Co-walker, spell-like ability</td>

      </tr>

      <tr>

      <td>9</td>

      <td>+12</td>

      <td>+12</td>

      <td>+12</td>

      <td>+8</td>

      <td>Smite threat 3/day, spell-like ability</td>

      </tr>

      </tbody>

      </table>

      <hr>

      <p><br><b>CR:</b> The guardian spirit’s CR increases based on the level of
      spell used to summon it, as noted on the Conjured Guardian table on page
      27.<p><b>Armor Class:</b> The guardian spirit’s natural armor bonus
      increases based on the level of spell used to summon it, as noted on the
      Conjured Guardian table.<p><b>Hit Dice:</b> The guardian spirit’s Hit Dice
      increase based on the level of spell used to summon it, as noted on the
      Conjured Guardian table. It gains appropriate skill points, feats, ability
      score increases, base attack bonus, and base saving throw advancements for
      its increased Hit Dice.<p><b>Defensive Abilities:</b> The guardian spirit
      has an amount of spell resistance equal to 11 + its CR unless the base
      creature’s SR was higher.<p><b>Ability Scores:</b> The guardian spirit’s
      Charisma score becomes 18 unless the base creature’s Charisma score was
      higher. Each of the guardian spirit’s ability scores increases when it’s
      summoned by higher-level spells, as noted on the Conjured Guardian table
      above.<p><b>Special Attacks:</b> If the guardian spirit has extraordinary
      or supernatural abilities that deal hit point damage measured in dice, the
      number of dice increases by an amount equal to the level of spell used to
      conjure it – 3. If the ability requires at least a standard action to
      activate and has an instantaneous duration, the damage increases by an
      additional die.<p><b>Special:</b> The guardian spirit gains a smite and
      additional special abilities as noted on the table.<p><em>Smite Threat
      (Su):</em> Once per day as a swift action, the guardian spirit can add its
      Charisma bonus on attack rolls and its HD on damage rolls against a foe
      that currently threatens its ward or has attacked the ward within the past
      24 hours; this smite persists until the target is dead or the summoning of
      the guardian spirit ends. If the spirit is summoned by a 6th-level spell,
      it can use smite threat an additional time per day, and if the spirit is
      summoned by a 9th-level spell, it can use smite threat a third time per
      day.<p><b>Spell-Like Abilities:</b> A guardian spirit’s caster level for
      its spell-like abilities is equal to its Challenge Rating + 1, or to the
      base creature’s caster level, whichever is higher. It can cast guidance at
      will. For every spell level of the conjuration spell used to call or
      summon it (such as planar ally, planar binding, or summon monster if the
      summoner has the Summon Guardian Spirit feat), the guardian spirit gains
      access to one additional spell-like ability of the ward’s choice from the
      following list:<p><em>Spell Level 3: </em>Chill touch, ill omenAPG,
      protection from chaos/evil/good/law (choose one; its alignment descriptor
      must oppose the guardian spirit’s alignment).<br><em>Spell Level 4:</em>
      Call lightning, detect thoughts, invisibility.<br><em>Spell Level 5:</em>
      Cure serious wounds, dispel magic, shout.<br><em>Spell Level 6:</em> Call
      lightning storm, death ward, freedom of movement.<br><em>Spell Level
      7:</em> Break enchantment, breath of life, contagious
      flameAPG.<br><em>Spell Level 8: </em>Cloak of dreamsAPG, greater heroism,
      sunbeam.<br><em>Spell Level 9:</em> Greater shout, power word blind,
      regenerate.<p>Each chosen spell-like ability is available once per
      day.<p><em>Fated Guardian (Su): </em>When conjured by a 4th-level or
      higher spell, a guardian spirit can protect the destiny of another
      creature within 30 feet as a standard action once per day. For 1 round,
      any time the creature makes an attack or attempts a saving throw, it rolls
      twice and takes the better result.<p><em>Co-Walker (Sp):</em> When
      conjured by an 8th-level or higher spell, a guardian spirit can assume the
      shape of its ward as if with alter self, except it can appear to be only
      the ward (even if the ward is not of a creature type or size that can
      normally be assumed with alter self) and it gains a +10 bonus on Disguise
      checks to appear to be the ward.</p>
  subType: template
type: feat

