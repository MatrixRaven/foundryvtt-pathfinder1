_id: 31gYYBIdKKvV51Vr
_key: '!items!31gYYBIdKKvV51Vr'
img: systems/pf1/icons/skills/violet_07.jpg
name: Nightmare Creature
system:
  changes:
    - _id: h854arww
      formula: '4'
      target: dex
      type: untyped
    - _id: zd1br2yn
      formula: '2'
      target: int
      type: untyped
    - _id: or3slbq1
      formula: '4'
      target: cha
      type: untyped
    - _id: aljrirfi
      formula: '4'
      target: skill.int
      type: racial
    - _id: 3kiitvs0
      formula: '4'
      target: skill.ste
      type: racial
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Both<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Nightmare creatures have an
      unnatural link to the most terrifying parts of the Dimension of Dreams,
      allowing them to turn others’ dreams into nightmares and sow fear even in
      the waking world. Corrupted by their power, they become evil and use their
      abilities to torment their enemies and abuse creatures weaker than
      themselves. Eventually this dream connection corrupts the creature’s
      appearance into a bizarre caricature of its original form.<p>A nightmare
      creature uses its ability to control dreams to confuse and frighten its
      target with horrendous imagery— visions of failure or betrayal and
      horrific scenes of murder and death. A nightmare creature may even allow
      the target to think it is in control of the dream or has awakened from a
      nightmare, only to snatch away that hope and send its target into a
      downward spiral of misery and self-doubt. The most wicked nightmare
      creatures tend to become ghosts if slain, returning again and again to
      haunt their chosen victims.<p>“Nightmare creature” is an acquired or
      inherited template that can be added to any creature with Intelligence and
      Charisma scores of at least 6 (referred to hereafter as the base
      creature). Most nightmare creatures were once aberrations, fey, humanoids,
      or outsiders. A nightmare creature uses the base creature’s statistics and
      abilities except as noted here. If the base creature has 10 or more Hit
      Dice, it instead becomes a nightmare lord (see below).<p><b>CR:</b> Same
      as the base creature +1.</p>

      <p><b>Alignment:</b> Any evil.</p>

      <p><b>Type:</b> If the base creature is an outsider, it gains the evil
      subtype.</p>

      <p><b>Senses:</b> A nightmare creature gains darkvision 120
      feet.<br>Defensive Abilities: A nightmare creature gains DR 5/good or
      silver and the following defensive abilities.<p><em>Feign Death (Ex):
      </em>Whenever a nightmare creature is unconscious, it appears dead. A
      conscious nightmare creature can also make itself appear dead as an
      immediate action. Any creature that physically interacts with a nightmare
      creature feigning death must succeed at a Heal check or Will saving throw
      (DC 10 + 1/2 the nightmare creature’s Hit Dice + the nightmare creature’s
      Intelligence or Charisma modifier, whichever is higher) to recognize it is
      actually alive.<p><em>Illusion Resistance (Ex): </em>A nightmare creature
      automatically disbelieves illusions (no saving throw required) and has a
      +4 bonus on saving throws to resist illusion effects.<p><em>Regeneration 5
      (Ex): </em>Good-aligned weapons, silver weapons, and spells with the good
      descriptor cause a nightmare creature’s regeneration to stop functioning
      for 1 round.<p><b>Speed:</b> Same as the base creature. If the base
      creature does not have a fly speed, the nightmare creature gains a fly
      speed<br>of 10 (perfect maneuverability) as a supernatural ability.</p>

      <p><b>Special Attacks:</b> A nightmare creature gains several special
      attacks. Save DCs are equal to 10 + 1/2 the nightmare creature’s Hit Dice
      + its Charisma modifier unless otherwise noted. The nightmare creature’s
      caster level is equal to its total Hit Dice (or the caster level of the
      base creature’s spell-like abilities, whichever is higher).<p><em>Fear
      Aura (Su): </em>All creatures within a 60-foot radius that see or hear a
      nightmare creature must succeed at a Will save or be shaken for as long as
      they are within the aura. Whether or not the save is successful, that
      creature cannot be affected again by the same nightmare creature’s fear
      aura for 24 hours. This is a mind-affecting fear affect.<p><em>Frightful
      Presence (Su): </em>This ability activates when the nightmare creature
      charges, attacks during a surprise round, or succeeds at a DC 15
      Intimidate or Perform check. Its frightful presence has a range of 30
      feet.<p><em>Night Terrors (Su): </em>Once a nightmare creature enters a
      target’s mind with its dream or nightmare spell-like ability, it can
      attempt to control the target’s dream. If the target fails a Will saving
      throw, it remains asleep and trapped in the dream world with the nightmare
      creature. Thereafter, the nightmare creature controls all aspects of the
      dream. Each hour that passes, the target can attempt another saving throw
      to try to awaken (it automatically awakens after 8 hours or if the
      nightmare creature releases it). The target takes 1d4 points of Charisma
      damage each hour it is trapped in the dream; if it takes any Charisma
      damage, it is fatigued and unable to regain arcane spells for the next 24
      hours. The target dies if this Charisma damage equals or exceeds its
      actual Charisma score.<p><b>Spell-Like Abilities:</b> A nightmare creature
      gains the following spell-like abilities: Constant—protection from good;
      3/day—detect thoughts, dream, nightmare, suggestion; 1/day—shadow
      walk.<p><b>Ability Scores:</b> Dex +4, Int +2, Cha +4.</p>

      <p><b>Skills:</b> A nightmare creature gains a +4 racial bonus on
      Intimidate and Stealth checks.</p>
  subType: template
type: feat

