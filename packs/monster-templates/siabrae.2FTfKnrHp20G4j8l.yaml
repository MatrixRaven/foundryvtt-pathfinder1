_id: 2FTfKnrHp20G4j8l
_key: '!items!2FTfKnrHp20G4j8l'
img: systems/pf1/icons/skills/violet_07.jpg
name: Siabrae
system:
  changes:
    - _id: yz53naur
      formula: '(@ac.natural.total >= 10) ? 0 : (10 - @ac.natural.total)'
      target: nac
      type: untyped
    - _id: 355oedq0
      formula: '2'
      target: str
      type: untyped
    - _id: buia6naf
      formula: '2'
      target: wis
      type: untyped
    - _id: 5iyvx1fn
      formula: '2'
      target: cha
      type: untyped
    - _id: diibrc1u
      formula: '8'
      target: skill.sen
      type: racial
    - _id: hdne6e5c
      formula: '8'
      target: skill.per
      type: racial
    - _id: etrqx19t
      formula: '8'
      target: skill.ste
      type: racial
  crOffset: '2'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>When druids are faced with threats
      to the natural world, they are steadfast and, at times, relentless in
      their defense of the land. Even in the face of overwhelming odds—an
      incursion of demons from the Abyss, a creeping plague of necromantic
      corruption, an unstoppable blight of magical radiation, or a similar
      supernatural threat to the natural world—some sects of druids refuse to
      give up or abandon their duties. In these tragic cases, the desperate
      druids adopt the blasphemous tactic of accepting the corruption into
      themselves and becoming powerful undead guardians. They fight on not only
      against the original source of the corruption, but against all living
      creatures, for these druids become siabraes, and are filled with
      bitterness and hatred for all others—particularly other druids, whom they
      regard as cowards. Siabraes do not form spontaneously; they arise only as
      the result of the horrific <a
      href="https://aonprd.com/OccultRituals.aspx?ItemName=Welcome%20the%20Blighted%20Soul">Welcome
      the Blighted Soul</a> ritual.<p>“Siabrae” is an acquired template that can
      be added to any druid who successfully performs the <a
      href="https://aonprd.com/OccultRituals.aspx?ItemName=Welome%20the%20Blighted%20Soul">welcome
      the blighted soul</a> ritual (hereafter referred to as the base creature).
      A siabrae can’t have the <a
      href="https://aonprd.com/ArchetypeDisplay.aspx?FixedName=Druid%20Blight%20Druid">blight
      druid</a> archetype. A siabrae retains all the base creature’s statistics
      and special abilities except as noted here.<p><b>CR:</b> Base creature’s
      CR + 2.<p><b>Alignment:</b> Neutral evil.<p><b>Type:</b> The creature’s
      type changes to undead with the earth subtype. Do not recalculate BAB,
      saves, or skill ranks.<p><b>Senses:</b> A siabrae gains darkvision and
      tremorsense, both with a range of 60 feet.<p><b>Armor Class:</b> A siabrae
      has a +10 natural armor bonus or the creature’s normal bonus, whichever is
      better.<p><b>Hit Dice:</b> Change the creature’s racial Hit Dice to d8s.
      All Hit Dice derived from class levels are unchanged. As an undead, a
      siabrae uses its Charisma modifier to determine its bonus hit points
      (rather than using its Constitution modifier).<p><b>Defensive
      Abilities:</b> In addition to all the abilities granted by its <a
      href="https://aonprd.com/UMR.aspx?ItemName=Undead%20Traits">undead
      traits</a>, a siabrae gains channel resistance +4, DR 10/ adamantine and
      bludgeoning, and immunity to fire. A siabrae also gains the following
      defensive ability.<p><em>Blighted Rebirth (Su):</em> When a siabrae is
      destroyed, it can attempt a DC 20 Fortitude save in order to avoid this
      end. The siabrae automatically succeeds at this saving throw if it is in
      contact with blighted or diseased terrain. On a successful save, the
      siabrae’s body crumbles to dust as the blighted earth absorbs its essence.
      Its enduring essence begins forming a new body in a random location within
      1d10 miles (this new location must contain a mass of unworked stone large
      enough for the siabrae’s body to form within). This process takes 1d10
      days, after which the siabrae emerges from the stone with a peal of
      thunder, though without any of its gear.<p><b>Speed:</b> A siabrae gains a
      burrow speed equal to its land speed, as well as the <a
      href="https://aonprd.com/UMR.aspx?ItemName=Earth%20Glide">earth glide</a>
      ability.<p><b>Attacks:</b> A siabrae grows a pair of stony antlers from
      its skull, granting it a <a
      href="https://aonprd.com/UMR.aspx?ItemName=Natural%20Attack">gore attack
      that deals damage based on the siabrae’s size</a>, but as if it were one
      size category larger than its actual size. This gore attack is always a
      primary attack, even when the siabrae also uses weapons. If the siabrae
      wishes, it can retain these antlers in any form it assumes via wild shape.
      Shards of the stony antlers break off in wounds—a siabrae’s antlers
      constantly replenish themselves as these shards break off. A creature
      damaged by a siabrae’s gore attack must succeed at a Fortitude save (DC =
      10 + 1/2 the siabrae’s HD + the siabrae’s Charisma modifier) or turn to
      stone permanently.<p><b>Special Attacks and Abilities:</b> A siabrae
      retains all the special attacks and abilities of the base creature. If it
      had the ability to use <a
      href="https://aonprd.com/ClassDisplay.aspx?ItemName=Druid">wild shape</a>,
      it retains this ability, but it can assume only the form of creatures that
      cannot fly. Any form it assumes (via wild shape or polymorph effects) and
      any creature it summons appears diseased, malnourished, or even in an
      advanced state of decay, although these are cosmetic effects; they do not
      impact actual game statistics. In addition, a siabrae gains the following
      special attacks.<p><em>Blight Mastery (Su):</em> Any of a siabrae’s spells
      or effects that would normally be restricted to affecting animals can also
      affect undead animals.<p><em>Blightbond (Ex):</em> A siabrae has an unholy
      bond with the blighted earth. It loses any animal companion or access to
      domains it had from its druidic nature bond ability. In place of nature
      bond, the siabrae’s close ties to the blighted landscape grant it one of
      the following cleric domains: <a
      href="https://aonprd.com/DomainDisplay.aspx?ItemName=Animal">Animal</a>,
      <a href="https://aonprd.com/DomainDisplay.aspx?ItemName=Death">Death</a>,
      <a
      href="https://aonprd.com/DomainDisplay.aspx?ItemName=Destruction">Destruction</a>,
      <a href="https://aonprd.com/DomainDisplay.aspx?ItemName=Earth">Earth</a>,
      <a
      href="https://aonprd.com/DomainDisplay.aspx?ItemName=Madness">Madness</a>,
      or <a
      href="https://aonprd.com/DomainDisplay.aspx?ItemName=Repose">Repose</a>.
      The blightbond ability otherwise functions the same as nature
      bond.<p><b>Ability Scores:</b> Str +2, Wis +2, Cha +2. Being undead, a
      siabrae has no Constitution score.</p>

      <p><b>Skills:</b> A siabrae gains a +8 racial bonus on Perception, Sense
      Motive, and Stealth checks. A siabrae always treats Intimidate, Knowledge
      (planes), Knowledge (religion), Sense Motive, and Stealth as class skills.
      Otherwise, a siabrae’s skills are the same as those of the base creature.
      Feats: A siabrae gains <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Toughness">Toughness</a>
      as a bonus feat.</p>
  subType: template
type: feat

